import unittest
import logger.logger as L
import spiders.ebilet as E


class LoggerTest(unittest.TestCase):
    def test_log(self):
        to_be_logged = 'LoggerTest Unittest'
        assert L.log_html(to_be_logged) == len(to_be_logged)
