import unittest
import spiders.ebilet as E
import logger.logger as L


class EbiletTest(unittest.TestCase):
    def test_simple_get(self):
        raw_html = E.simple_get('http://www.ebilet.pl')
        assert len(raw_html) > 0

    def test_log_response(self):
        raw_html = E.simple_get('http://www.ebilet.pl')
        assert E.log_resp(raw_html)[1] > 0

    def test_log_meskie(self):
        raw_html = E.simple_get('https://www.ebilet.pl/muzyka/festiwale/meskie-granie/')
        assert E.log_resp(raw_html)[1] > 0

    def test_log_meskie_sell(self):
        raw_html = E.simple_get('https://sklep.ebilet.pl/7881299347898386')
        assert E.log_resp(raw_html)[1] > 0

    def test_find_ticket_sell(self):
        raw_html = E.simple_get('https://www.ebilet.pl/muzyka/festiwale/meskie-granie/')
        assert len(E.find_ticket_sell(raw_html)) > 0

    def test_log_once(self):
        raw_html = E.simple_get('https://www.ebilet.pl/teatr/musicale/once/')
        log_info = E.log_resp(raw_html, 'Once')
        ticket_sells = E.find_ticket_sell(raw_html)
        ticket_sell = ticket_sells[-1][1]
        print(ticket_sell)
        ticket_sell_html = E.simple_get(ticket_sell)
        log_info = E.log_resp(ticket_sell_html, 'Sell')
        assert log_info[1] > 0
