import re
from contextlib import closing

from bs4 import BeautifulSoup
from requests import get
from requests.exceptions import RequestException

from logger.logger import log_html


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_resp(resp):
                return resp.content
            return None
    except RequestException as req_ex:
        print('Error during request {0}: {1}'.format(url, req_ex))


def is_good_resp(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def log_resp(resp, page_name=''):
    soup = BeautifulSoup(resp, 'html.parser')
    return log_html(soup.prettify(), page_name)


def find_ticket_sell(resp):
    """
    Retrieves a list of ticket sell links for later use.
    """
    parser = BeautifulSoup(resp, 'html.parser')

    links = []
    descs = []
    dates = []
    sell_start_dates = []
    sell_end_dates = []
    for script in parser.select('script'):
        links.extend(re.findall(r'https://sklep.ebilet.pl/\d+',
                                script.get_text()))
        dates.extend(list(
            map(lambda x: x[8:-1],
                re.findall(r'"Date":"\d+-\d+-\d+T\d+:\d+:\d+"',
                           script.get_text()))
            ))
        sell_start_dates.extend(list(
            map(lambda x: x[17:-1],
                re.findall(r'"SellStartDate":"\d+-\d+-\d+T\d+:\d+:\d+"',
                           script.get_text()))
            ))
        sell_end_dates.extend(list(
            map(lambda x: x[15:-1],
                re.findall(r'"SellEndDate":"\d+-\d+-\d+T\d+:\d+:\d+"',
                           script.get_text()))
            ))
        descs.extend(list(
            map(lambda x: x[13:-1],
                re.findall(r'"PlaceCity":"\w+"', script.get_text()))
            )
        )
    system_sells = list(zip(descs, links,
                            dates, sell_start_dates,
                            sell_end_dates))
    return system_sells
