import os
from datetime import datetime


def log_html(resp, page_name=''):
    """
    Logs the response from simple_get into a txt file - for further analysis
    """
    log_path = os.path.join('logs/', page_name)
    curr_datetime = datetime.now().strftime('%y%m%d_%H%M%S')
    f_name = ''.join([log_path, '/',  curr_datetime, '.html'])
    f_lenght = 0
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    with open(f_name, 'w') as f_log:
        f_lenght = f_log.write(resp)
        print('characters written: {}'.format(f_lenght))
        return (f_name, f_lenght)
    return('', 0)


def log(resp, page_name=''):
    """
    Logs the string into a txt file - for further analysis
    """
    log_path = os.path.join('logs/', page_name)
    curr_datetime = datetime.now().strftime('%y%m%d_%H%M%S')
    f_name = ''.join([log_path, '/',  curr_datetime])
    f_lenght = 0
    if not os.path.exists(log_path):
        os.mkdir(log_path)
    with open(f_name, 'w') as f_log:
        f_lenght = f_log.write(resp)
        print('characters written: {}'.format(f_lenght))
        return (f_name, f_lenght)
    return('', 0)
